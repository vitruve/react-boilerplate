## React Boilerplate

React Boilerplate with React Router, Material UI, Webpack and Hot Module Replacement.

This is a simple React boilerplate with:

- [React Router](https://www.npmjs.com/package/react-router)
- [Material UI](http://www.material-ui.com)
- [Webpack](https://www.npmjs.com/package/webpack)
- [Hot Module Replacement](https://webpack.github.io/docs/hot-module-replacement-with-webpack.html)

## Usage

```
$ git clone https://Vitruve@bitbucket.org/Vitruve/react-boilerplate.git
$ cd react-boilerplate
$ npm install
$ npm start
```
...and go to: http://localhost:3000