import React from 'react';
import {Link} from 'react-router';

import RaisedButton from 'material-ui/lib/raised-button';

class SomePage extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h1>Some Page</h1>
        <h2>The parameter is: {this.props.params.someparam}</h2>
        <Link to={'/'}>
          <RaisedButton label="Go back Home" primary={true} />
        </Link>
      </div>
    )
  }
};

export default SomePage;
